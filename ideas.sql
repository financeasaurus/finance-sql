# Grab amount spent by transaction type - Add user_id, month, and year (the last two will be optional)
SELECT 
	sum(amount) as 'sum',
	transaction_type_id
FROM transactions
GROUP BY transaction_type_id;

# Get the average values - same params as above
SELECT
	AVG(amount) as 'average',
	transaction_type_id
FROM transactions
GROUP BY transaction_type_id;

# This will sum by MONTH
SELECT 
	sum(amount) as 'sum',
	MONTH(date) as "month"
FROM transactions
GROUP BY MONTH(date);

# Now sort by week
SELECT 
	sum(amount) as 'sum',
	WEEK(date) as "week"
FROM transactions
GROUP BY WEEK(date);

# Now sort by week and transaction type
SELECT 
	sum(amount) as 'sum',
	WEEK(date) as "week",
	transaction_type_id
FROM transactions
GROUP BY WEEK(date), transaction_type_id
ORDER BY WEEK(date);
