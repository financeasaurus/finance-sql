CREATE TABLE `gas_transactions` (
  `gas_id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(18,2) DEFAULT NULL,
  `milage` decimal(18,1) DEFAULT NULL,
  `gallons` decimal(18,3) DEFAULT NULL,
  `transaction_id` int(11) NOT NULL,
  `car_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gas_id`),
  KEY `gas_transaction_id` (`transaction_id`),
  KEY `gas_car_id` (`car_id`),
  CONSTRAINT `gas_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
  CONSTRAINT `gas_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Keeping track of gas information'