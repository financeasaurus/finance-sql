CREATE TABLE `transaction_types` (
  `transaction_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_type_id`),
  KEY `transaction_types_users_FK` (`user_id`),
  CONSTRAINT `transaction_types_users_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='The type a transaction can be. When user_id is null then that data is available for all users. They will be the defaults. New ones will be user created.'