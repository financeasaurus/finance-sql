CREATE TABLE `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT 0,
  `amount` decimal(18,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `transaction_type_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_account_id` (`account_id`),
  KEY `transaction_type_id` (`transaction_type_id`),
  CONSTRAINT `transaction_account_id` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_types` (`transaction_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='Deposits or expenses into an account'