CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_transaction_type`(
	IN `user_id` INT,
	IN `type_id` INT
)
    COMMENT 'Update a transaction_type that is user_specific'
BEGIN
	# I'm paranoid about a user deleting a core type so I go overboard to make sure that doesn't happen 
	DELETE 
	FROM transaction_types
	WHERE transaction_types.transaction_type_id = type_id AND transaction_types.user_id = user_id AND transaction_types.user_id IS NOT NULL;
END