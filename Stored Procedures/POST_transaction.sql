CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`POST_transaction`(
	IN `amount` DECIMAL(18,2),
	IN `acccount_id` INT,
	IN `type_id` INT,
	IN `transaction_date` DATE,
	IN `transaction_description` VARCHAR(255)
)
    MODIFIES SQL DATA
    COMMENT 'Create new transaction'
BEGIN
	INSERT INTO transactions (account_id, amount, date, description, transaction_type_id)
	VALUES (account_id, amount, transaction_date, transaction_description, type_id);
	SET @new_transaction_id := LAST_INSERT_ID();
	
	# If type matches gas then insert gas transaction
	IF type_id IN (SELECT types.transaction_type_id as 'transaction_type_id' from transaction_types AS types WHERE types.name LIKE 'gas%') THEN
		INSERT INTO gas_transactions (transaction_id) 
		VALUES (@new_transaction_id);
	END IF;
		
	SELECT @new_transaction_id AS 'transaction_id';
END