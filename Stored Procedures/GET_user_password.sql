CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_user_password`(
	IN `user_id` INT
)
    COMMENT 'Return the password hash based on the user_id'
BEGIN
	SELECT password as 'password'
	FROM users
	WHERE users.user_id = user_id;
END