CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_user`(
	IN `user_id` INT
)
    COMMENT 'Deletes an account based on the user_id'
BEGIN
	# We have to delete all gas transaction first, since they are tied to transactions
	DELETE gas
	FROM gas_transactions AS gas
		inner join transactions AS trans ON trans.transaction_id = gas.transaction_id
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.user_id = user_id;
	
	# We have to now delete all transactions for the account
	DELETE trans
	FROM transactions AS trans
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.user_id = user_id;

	DELETE 
	FROM transaction_types
	WHERE transaction_types.user_id = user_id AND transaction_types.user_id IS NOT NULL;
	
	# Now we can finally delete the account itself
	DELETE
	FROM accounts
	WHERE accounts.user_id = user_id;

	DELETE
	FROM users
	WHERE users.user_id = user_id;
END