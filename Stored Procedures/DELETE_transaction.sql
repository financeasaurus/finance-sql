CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_transaction`(
	IN `transaction_id` INT,
	IN `user_id` INT
)
    COMMENT 'Deletes a transaction based on the transaction_id and user'
BEGIN
	DELETE trans
	FROM transactions as trans
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE trans.transaction_id = transaction_id AND accounts.user_id = user_id;
END