CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_gas_transactions`(
	IN `user_id` INT,
	IN `year` INT
)
    COMMENT 'Get the gas transaction by the current year'
BEGIN
	SELECT 
		gas.*,
		CASE WHEN (gas.price * gas.gallons) IS NULL THEN trans.amount ELSE (gas.price * gas.gallons) END AS total,
		(gas.milage / gas.gallons) AS mpg,
		trans.account_id AS account_id,
		trans.date AS date
	FROM gas_transactions AS gas
		left join transactions AS trans ON trans.transaction_id = gas.transaction_id
		left join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.user_id = user_id && (year IS NULL || YEAR(trans.date) = year)
	ORDER BY trans.date DESC;
END