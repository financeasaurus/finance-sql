CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`PUT_gas_transaction`(
	IN `gas_id` INT,
	IN `car_id` INT,
	IN `account_id` INT,
	IN `gas_price` DECIMAL(18,2),
	IN `gas_gallons` DECIMAL(18,3),
	IN `gas_milage` DECIMAL(18,1),
	IN `transaction_date` DATE,
	IN `transaction_id` INT
)
    COMMENT 'Update a gas transaction which also updates the corresponding transaction'
BEGIN
	# Update the gas transaction and its corresponding regular transaction
	UPDATE gas_transactions AS gas
	SET gas.car_id = car_id,
		gas.price = gas_price,
		gas.milage = gas_milage,
		gas.gallons = gas_gallons
	WHERE gas.gas_id = gas_id;
		
	UPDATE transactions AS trans
	SET trans.account_id = account_id,
		trans.amount = (gas_price * gas_gallons),
		trans.date = transaction_date
	WHERE trans.transaction_id = transaction_id;
END