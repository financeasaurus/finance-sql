CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_account`(
	IN `accountId` INT,
	IN `userId` INT
)
    COMMENT 'Get an account info with the account ID'
BEGIN
	SELECT 
		accounts.account_id,
		accounts.name,
		accounts.description, 
		SUM(trans.amount) as balance
	FROM accounts
		left join transactions as trans ON trans.account_id = accounts.account_id
	WHERE accounts.account_id = accountId AND accounts.user_id = userId
	GROUP BY accounts.account_id;
END