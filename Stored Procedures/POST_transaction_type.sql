CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`POST_transaction_type`(
	IN `user_id` INT,
	IN `type_name` VARCHAR(20),
	IN `type_description` VARCHAR(255)
)
    COMMENT 'Create a new transaction_type that is user_specific'
BEGIN
	INSERT INTO transaction_types (name, description, user_id)
	VALUES (type_name, type_description, user_id);
	SELECT LAST_INSERT_ID() as 'transaction_type_id';
END