CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_gas_transaction`(
	IN `user_id` INT,
	IN `gas_id` INT,
	IN `transaction_id` INT
)
    COMMENT 'Deletes a gas transaction and its corresponding regular transaction'
BEGIN
	DELETE gas
	FROM gas_transactions as gas
	WHERE gas.gas_id = gas_id;

	DELETE trans
	FROM transactions as trans
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE trans.transaction_id = transaction_id AND accounts.user_id = user_id;
END