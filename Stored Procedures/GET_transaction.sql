CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_transaction`(
	IN `userId` INT,
	IN `transaction_id` INT
)
    COMMENT 'Get a transaction'
BEGIN
	SELECT 
		trans.*,
		tt.name AS transaction_type_name,
		accounts.account_id AS account_id,
		accounts.name AS account_name
	FROM transactions AS trans
		left join accounts ON accounts.account_id = trans.account_id
		left join transaction_types AS tt ON tt.transaction_type_id = trans.transaction_type_id 
	WHERE accounts.user_id = userId && trans.transaction_id = transaction_id;
END