CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`POST_account`(
	IN `userId` INT,
	IN `accountDescription` VARCHAR(255),
	IN `accountName` VARCHAR(255)
)
    COMMENT 'Create an account'
BEGIN
	INSERT INTO accounts (user_id, name, description) VALUES (userId, accountName, accountDescription);
	SET @new_account_id := LAST_INSERT_ID();
	SELECT @new_account_id as 'account_id';
END