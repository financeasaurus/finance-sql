CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_account`(
	IN `userId` INT,
	IN `accountId` INT
)
    COMMENT 'Deletes an account based on the account_id'
BEGIN
	# We have the user_id included just for extra security.
	
	# We have to delete all gas transaction first, since they are tied to transactions
	DELETE gas
	FROM gas_transactions AS gas
		inner join transactions AS trans ON trans.transaction_id = gas.transaction_id
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.account_id = accountId AND accounts.user_id = userId;
	
	# We have to now delete all transactions for the account
	DELETE trans
	FROM transactions AS trans
		inner join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.account_id = accountId AND accounts.user_id = userId;	 
	
	# Now we can finally delete the account itself
	DELETE
	FROM accounts
	WHERE accounts.account_id = accountId AND accounts.user_id = userId;
END