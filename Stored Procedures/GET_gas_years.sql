CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_gas_years`(
	IN `userId` INT
)
    COMMENT 'Returns all the years the user has gas transactions'
BEGIN
	SELECT DISTINCT YEAR(trans.date) AS year 
	FROM gas_transactions AS gas
		left join transactions AS trans ON trans.transaction_id = gas.transaction_id
		left join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.user_id = userId;
END