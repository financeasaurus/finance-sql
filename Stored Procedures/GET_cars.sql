CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_cars`(
	IN `user_id` INT
)
    COMMENT 'Return all cars linked to the user'
BEGIN
	SELECT *
	FROM cars
	WHERE cars.user_id = user_id;
END