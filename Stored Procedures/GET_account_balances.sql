CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_account_balances`(
	IN `user_id` INT,
	IN `account_id` INT
)
    COMMENT 'Get the history of balances for the specified account'
BEGIN
	# Need to handle nulls
	SELECT 
		(SELECT 
			SUM(trans2.amount)
			FROM transactions as trans2
			WHERE trans2.date <= trans.date AND trans2.account_id = account_id
		) as balance,
		trans.date,
		accounts.account_id
	FROM transactions as trans
		left join accounts ON accounts.account_id = trans.account_id
	WHERE trans.account_id = account_id AND accounts.user_id = user_id
	GROUP BY trans.date DESC;
END