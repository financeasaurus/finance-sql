CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_transaction_types`(
	IN `user_id` INT
)
    COMMENT 'Return list of all transaction types belonging to the user, both core and user created'
BEGIN
	SELECT *
	FROM transaction_types
	WHERE transaction_types.user_id = user_id || transaction_types.user_id IS NULL;
END