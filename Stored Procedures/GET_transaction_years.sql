CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_transaction_years`(
	IN `userId` INT
)
    COMMENT 'Get all the years found in a user''s transactions'
BEGIN
	SELECT DISTINCT YEAR(trans.date) AS year 
	FROM transactions AS trans
		left join accounts ON accounts.account_id = trans.account_id
	WHERE accounts.user_id = userId;
END