CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_accounts`(
	IN `userId` INT
)
    COMMENT 'Return all accounts for a user'
BEGIN
	SELECT 
		accounts.account_id,
		accounts.name,
		accounts.description, 
		(CASE WHEN SUM(trans.amount) IS NULL THEN 0 ELSE SUM(trans.amount) END) as balance
	FROM accounts
		left join transactions as trans ON trans.account_id = accounts.account_id
	WHERE accounts.user_id = userId
	GROUP BY accounts.account_id;
END