CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`DELETE_car`(
	IN `user_id` INT,
	IN `car_id` INT
)
    COMMENT 'Return a specific car'
BEGIN
	DELETE
	FROM cars
	WHERE cars.car_id = car_id && cars.user_id = user_id;
END