CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`PUT_transaction_type`(
	IN `user_id` INT,
	IN `type_name` VARCHAR(20),
	IN `type_description` VARCHAR(255),
	IN `type_id` INT
)
    COMMENT 'Update a transaction_type that is user_specific'
BEGIN
	UPDATE transaction_types
	SET name = type_name,
		description = type_description
	WHERE transaction_types.transaction_type_id = type_id AND transaction_types.user_id = user_id AND transaction_types.user_id IS NOT NULL;
END