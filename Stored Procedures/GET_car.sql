CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_car`(
	IN `user_id` INT,
	IN `car_id` INT
)
    COMMENT 'Return a specific car'
BEGIN
	SELECT *
	FROM cars
	WHERE cars.car_id = car_id && cars.user_id = user_id;
END