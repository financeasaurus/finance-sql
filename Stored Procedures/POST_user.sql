CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`POST_user`(
	IN `username` VARCHAR(50),
	IN `password_hash` TEXT
)
    MODIFIES SQL DATA
    COMMENT 'Creates a user and stores their info'
BEGIN
	INSERT INTO users (username, password) VALUES (username, password_hash);
	SELECT LAST_INSERT_ID() as 'user_id';
END