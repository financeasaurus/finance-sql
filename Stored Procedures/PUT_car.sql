CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`PUT_car`(
	IN `userId` INT,
	IN `car_name` VARCHAR(50),
	IN `car_id` INT
)
    COMMENT 'Create a new car for the user'
BEGIN
	# TODO: Add more car info
	UPDATE cars
	SET cars.car_name = car_name
	WHERE cars.car_id = car_id AND car_id.user_id = user_id;
END