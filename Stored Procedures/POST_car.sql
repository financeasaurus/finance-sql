CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`POST_car`(
	IN `user_id` INT,
	IN `car_name` VARCHAR(50)
)
    COMMENT 'Create a car'
BEGIN
	INSERT INTO cars (user_id, car_name)
	VALUES (user_id, car_name);
	SET @new_car_id := LAST_INSERT_ID();
	SELECT @new_car_id as 'car_id';
END