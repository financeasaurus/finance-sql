CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`PUT_account`(
	IN `userId` INT,
	IN `accountDescription` VARCHAR(255),
	IN `accountName` VARCHAR(255),
	IN `accountId` INT
)
    COMMENT 'Update an account'
BEGIN
	UPDATE accounts
	SET name = accountName,
		description = accountDescription
	WHERE account_id = accountId;
END