CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_transactions`(
	IN `user_id` INT,
	IN `year` INT,
	IN `transaction_type_id` INT
)
    COMMENT 'Get all transactions with optional year filter'
BEGIN
	SELECT 
		trans.*,
		tt.name AS transaction_type_name,
		accounts.account_id AS account_id,
		accounts.name AS account_name
	FROM transactions AS trans
		left join accounts ON accounts.account_id = trans.account_id
		left join transaction_types AS tt ON tt.transaction_type_id = trans.transaction_type_id 
	WHERE accounts.user_id = user_id && (YEAR(trans.date) = year || year IS NULL) && (trans.transaction_type_id = transaction_type_id || transaction_type_id IS NULL)
	ORDER BY trans.date DESC;
END