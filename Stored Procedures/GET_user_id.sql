CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_user_id`(
	IN `username` VARCHAR(255)
)
    COMMENT 'Return the user_id based on the username'
BEGIN
	SELECT user_id as 'user_id'
	FROM users
	WHERE BINARY users.username = username;
END