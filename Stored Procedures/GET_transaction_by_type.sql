CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`GET_transaction_by_type`(
	IN `userId` INT,
	IN `year` INT,
	IN `accountId` INT
)
    COMMENT 'Return the totals of each type of transaction for the user''s transactions'
BEGIN
	SELECT
		trans.transaction_type_id,
		COUNT(trans.transaction_type_id) as 'total'
	FROM transactions AS trans
		inner join accounts ON trans.account_id = accounts.account_id
		inner join users ON accounts.user_id = users.user_id
	WHERE users.user_id = userId AND (accounts.account_id = accountId OR accountId IS NULL) AND (YEAR(trans.date) = year OR year IS NULL)
	GROUP BY transaction_type_id
	ORDER BY transaction_type_id;
END