CREATE DEFINER=`db_user`@`%` PROCEDURE `finance`.`PUT_transaction`(
	IN `amount` DECIMAL(18,2),
	IN `acccount_id` INT,
	IN `type_id` INT,
	IN `transaction_date` DATE,
	IN `transaction_description` VARCHAR(255),
	IN `transaction_id` INT
)
    MODIFIES SQL DATA
    COMMENT 'Update a transaction'
BEGIN
	UPDATE transactions AS trans
	SET trans.account_id = acccount_id, 
		trans.amount = amount,
		trans.date = transaction_date,
		trans.description = transaction_description,
		trans.transaction_type_id = type_id
	WHERE trans.transaction_id = transaction_id;
		
	IF type_id IN (SELECT types.transaction_type_id FROM transaction_types AS types WHERE types.name LIKE 'gas%') THEN
		IF transaction_id NOT IN (SELECT gas.transaction_id AS 'transaction_id' FROM gas_transactions AS gas WHERE gas.transaction_id = transaction_id) THEN
			INSERT INTO gas_transactions (transaction_id)
			VALUES (transaction_id);
		END IF;
	ELSE
		DELETE FROM gas_transactions
		WHERE gas_transactions.transaction_id = transaction_id;
	END IF;
END